/*
 * typst-math-api, A simple API for rendering typst math
 * Copyright (c) 2023 Tyler Beckman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as path from "https://deno.land/std@0.184.0/path/mod.ts";

import { stripIndent } from "https://deno.land/x/proper_tags@2.0.0-beta.0/dist/proper-tags.js";

const PORT = Deno.env.get("PORT") ?? "8088";

const server = Deno.listen({ port: Number(PORT) });
console.log(`HTTP webserver running. Access it at: http://localhost:${PORT}/`);

for await (const conn of server) {
    serveHttp(conn);
}

async function serveHttp(conn: Deno.Conn) {
    const httpConn = Deno.serveHttp(conn);
    for await (const requestEvent of httpConn) {
        // Fuck cors
        if (requestEvent.request.method === "OPTIONS") {
            requestEvent.respondWith(
                new Response(null, {
                    status: 204,
                    headers: {
                        "Allow": "OPTIONS, GET",
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": "OPTIONS, GET"
                    }
                })
            )
        } else {
            // Actual code
            const url = new URL(requestEvent.request.url);
            if (requestEvent.request.method !== "GET") requestEvent.respondWith(
                new Response("Method not allowed", {
                    status: 405
                })
            );
            else if (!url.searchParams.has("code")) requestEvent.respondWith(
                new Response("Missing code query param", {
                    status: 422
                })
            );
            else {
                const tmpdir = await Deno.makeTempDir({ prefix: "typst-math-api" });
                const typFilePath = path.join(tmpdir, "math.typ");
                const pdfFilePath = path.join(tmpdir, "math.pdf");
                await Deno.writeFile(
                    typFilePath,
                    new TextEncoder().encode(stripIndent`
                        #set page(width: auto, height: auto, margin: 10pt)
                        ${(url.searchParams.get("theme") ?? "light") === "light"
                            ? "#set page(fill: white)"

                            : "#set page(fill: rgb(49, 51, 56))\n"
                            + "#set text(fill: rgb(219, 222, 225))"
                        }
                        
                        $ ${url.searchParams.get("code")!} $`
                    ));
                const typst = Deno.run({
                    cmd: ["typst", "compile", typFilePath, pdfFilePath],
                    stdin: "null",
                    stdout: "null",
                    stderr: "null"
                })
                const typstStatus = await typst.status();
                if (!typstStatus.success) requestEvent.respondWith(
                    new Response("Invalid code", {
                        status: 415,
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                            "Access-Control-Allow-Methods": "OPTIONS, GET"
                        }
                    })
                );
                else {
                    const pdftoppm = Deno.run({
                        cmd: ["pdftoppm", "-png", pdfFilePath, "math"],
                        cwd: tmpdir,
                        stdin: "null",
                        stdout: "null",
                        stderr: "null"
                    })
                    const pdftoppmStatus = await pdftoppm.status();
                    if (!pdftoppmStatus.success) requestEvent.respondWith(
                        new Response("Unable to convert pdf to png", {
                            status: 500,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Methods": "OPTIONS, GET"
                            }
                        })
                    );
                    else requestEvent.respondWith(
                        new Response(
                            await Deno.readFile(path.join(tmpdir, "math-1.png")),
                            {
                                headers: {
                                    "Content-Type": "image/png",
                                    "Access-Control-Allow-Origin": "*",
                                    "Access-Control-Allow-Methods": "OPTIONS, GET"
                                },
                                status: 200
                            }
                        )
                    )
                }

                await Deno.remove(tmpdir, { recursive: true })
            }
        }
    }
}