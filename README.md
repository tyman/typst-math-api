# typst-math-api

A very simple CORS-enabled API using `typst` and `pdftoppm` in order to accept typst math syntax and return a rendered PDF file. Port can be configured with the `PORT` environment variable if you want to use this I guess. This probably only works on linux because it doesn't take into account any of the commands having `.exe` file extensions.